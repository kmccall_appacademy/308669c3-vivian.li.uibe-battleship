class Board
  attr_accessor :grid
  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  def self.default_grid
    Array.new(10){Array.new(10,nil)}
  end

  def count
    cnt = 0
    @grid.each{|row|cnt += row.count(:s)}
    cnt
  end

  def populate_grid
    random_distribute_ships
  end

  def in_ranges?(pos)

  end

  def empty?(pos = nil)
    if pos.nil?
      count == 0
    else
      x, y = pos
      @grid[x][y].nil?
    end
  end

  def full?
    len = @grid.length
    @grid.each do |row|
      return false unless row.count(:s) == len
    end
    true
  end

  def place_random_ship
    raise "error" if full?
    pos = available[rand(available.length-1)]
    mark(pos, :s)
  end

  def available
    pos=[]
    @grid.each_with_index do |row, x|
      row.each_index do |y|
        pos << [x, y] if @grid[x][y].nil?
      end
    end
    pos
  end

  def won?
    count == 0
  end

  def mark(pos,symbol)
    x,y = pos
    @grid[x][y] = symbol
  end

  def display
    @grid
  end

  def [](pos)
    x, y = pos
    @grid[x][y]
  end

  def []=(pos, mark)
    x, y = pos
    @grid[x][y] = mark
  end



end
