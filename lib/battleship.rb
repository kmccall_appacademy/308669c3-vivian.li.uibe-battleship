class BattleshipGame
  attr_reader :board, :player

  def initialize(player,board)
    @player = player
    @board = board
  end

  def attack(pos)
    @board[pos] = :x
  end

  def count
    @board.count
  end

  def game_over?
    @board.won?
  end

  def display_status

  end

  def play

  end

  def play_turn
    attack(player.get_play)
  end
end
